#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>

//const int PORT = 8070;
const int BUFFER_SIZE = 1024;

// Функция для обработки сообщений от клиента в отдельном потоке
void receive_from_client(int new_sockfd, const std::string& key) {
    char buffer[BUFFER_SIZE];

    while (true) {
        int bytes_received = recv(new_sockfd, buffer, BUFFER_SIZE, 0);
        if (bytes_received <= 0) {
            // Если нет данных от клиента, то закрываем соединение
            std::cout << "Подключение с клиентом разорвано." << std::endl;
            break;
        }

        // Расшифровываем полученное сообщение от клиента с помощью XOR
        for (int i = 0; i < bytes_received; i++) {
            buffer[i] ^= key[i % key.length()];
        }

        // Выводим расшифрованное сообщение от клиента
        std::cout << std::endl << "Получено сообщение от клиента: " << buffer << std::endl;

        // Очищаем буфер
        memset(buffer, 0, BUFFER_SIZE);
    }

    // Закрываем сокет
    close(new_sockfd);
}

// Функция для отправки сообщений клиенту в отдельном потоке
void send_to_client(int new_sockfd, const std::string& key) {
    std::string message;

    while (true) {
        // Вводим сообщение для отправки клиенту
        std::cout << "Введите сообщение для отправки клиенту (для завершения введите 'exit'): ";
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::getline(std::cin, message);

        if (message == "exit") {
            // Если введено "exit", то закрываем соединение
            std::cout << "Закрытие подключения с клиентом." << std::endl;
            break;
        }

        // Шифруем сообщение перед отправкой клиенту с помощью XOR
        for (int i = 0; i < message.length(); i++) {
            message[i] ^= key[i % key.length()];
        }

        // Отправляем сообщение клиенту
        int bytes_sent = send(new_sockfd, message.c_str(), message.length(), 0);
        if (bytes_sent == -1) {
            std::cerr << "Ошибка при отправке сообщения клиенту!" << std::endl;
            exit(1);
        }
    }

    // Закрываем сокет
    close(new_sockfd);
}

int main() {
    int sockfd, new_sockfd;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_len;
    char buffer[BUFFER_SIZE];

    // Создаем сокет
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        std::cerr << "Ошибка при создании сокета!" << std::endl;
        exit(1);
    }
    std::cout << "Введите порт сервера: ";
    int server_port;
    std::cin >> server_port;
    // Устанавливаем параметры сервера
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(server_port);
    // Привязываем сокет к адресу и порту
if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
    std::cerr << "Ошибка при привязке сокета к адресу и порту!" << std::endl;
    exit(1);
}

// Слушаем входящие подключения
if (listen(sockfd, 10) == -1) {
    std::cerr << "Ошибка при прослушивании входящих подключений!" << std::endl;
    exit(1);
}

std::cout << "Сервер запущен и ожидает подключения..." << std::endl;

while (true) {
    // Принимаем входящее подключение
    client_len = sizeof(client_addr);
    new_sockfd = accept(sockfd, (struct sockaddr*)&client_addr, &client_len);
    if (new_sockfd == -1) {
        std::cerr << "Ошибка при принятии входящего подключения!" << std::endl;
        exit(1);
    }

    // Приветствие клиента
    std::cout << "Подключился клиент с IP-адресом: " << inet_ntoa(client_addr.sin_addr) << std::endl;

    // Вводим ключ для шифрования/расшифрования сообщений
    std::string key;
    std::cout << "Введите ключ для шифрования/расшифрования сообщений: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::getline(std::cin, key);

    // Запускаем отдельные потоки для приема и отправки сообщений
    std::thread receive_thread(receive_from_client, new_sockfd, key);
    std::thread send_thread(send_to_client, new_sockfd, key);

    // Ожидаем завершения работы потоков
    receive_thread.join();
    send_thread.join();
}

// Закрываем сокет
close(sockfd);

return 0;
}
