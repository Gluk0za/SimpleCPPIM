#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>

const int BUFFER_SIZE = 1024;

// Функция для шифрования и дешифрования сообщений с использованием XOR и ключа
void xor_encrypt_decrypt(char* buffer, int size, const std::string& key) {
    if (buffer == nullptr || size <= 0 || key.empty()) {
        std::cerr << "Ошибка: некорректные аргументы функции." << std::endl;
        return;
    }

    for (int i = 0; i < size; i++) {
        buffer[i] ^= key[i % key.length()];
    }
}

// Функция для чтения входящих сообщений от сервера
void receive_messages(int sockfd, const std::string& key) {
    char buffer[BUFFER_SIZE];
    while (true) {
        int bytes_received = recv(sockfd, buffer, BUFFER_SIZE, 0);
        if (bytes_received <= 0) {
            // Ошибка или сервер закрыл соединение, выходим из потока
            std::cout << std::endl << "Ошибка при чтении сообщения от сервера или сервер закрыл соединение." << std::endl;
            break;
        } else {
            // Дешифруем полученное сообщение
            xor_encrypt_decrypt(buffer, bytes_received, key);

            // Выводим полученное сообщение на экран
            buffer[bytes_received] = '\0';
            std::cout << std::endl << "Получено от сервера: " << buffer << std::endl;
        }
    }
}

int main() {
    // Ввод ключа с клавиатуры
    std::cout << "Введите ключ для шифрования/дешифрования: ";
    std::string key;
    std::getline(std::cin, key);
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        std::cout << "Ошибка при создании сокета." << std::endl;
        return 1;
    }
    sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    // Ввод адреса сервера с клавиатуры
    std::cout << "Введите адрес сервера: ";
    std::string server_ip;
    std::getline(std::cin,server_ip);
    server_addr.sin_addr.s_addr = inet_addr(server_ip.c_str());

    // Ввод порта с клавиатуры
    std::cout << "Введите порт сервера: ";
    int server_port;
    std::cin >> server_port;
    server_addr.sin_port = htons(server_port);

    if (connect(sockfd, (sockaddr*)&server_addr, sizeof(server_addr)) == -1) {
        std::cout << "Ошибка при подключении к серверу." << std::endl;
        return 1;
    }

    // Запускаем поток для чтения входящих сообщений от сервера
    std::thread receive_thread(receive_messages, sockfd, key);

    // Чтение и отправка сообщений
    while (true) {
        std::cout << "Введите сообщение для сервера (или 'exit' для завершения): ";
        std::string message;
        std::getline(std::cin, message);

        // Шифруем сообщение
        xor_encrypt_decrypt(&message[0], message.length(), key);

        send(sockfd, message.c_str(), message.length(), 0);

        if (message == "exit") {
            // Если введено "exit", закрываем соединение и выходим из цикла
            std::cout << "Завершение соединения..." << std::endl;
            break;
        }
    }

    // Ожидаем завершения потока чтения входящих сообщений
    receive_thread.join();

    // Закрываем сокет
    close(sockfd);

    return 0;
}
